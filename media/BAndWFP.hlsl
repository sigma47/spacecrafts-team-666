sampler2D RT : register(s0);

float4 BAndW_ps (float4 pos : POSITION, float2 iTexCoord : TEXCOORD0) : COLOR
{
	float3 rgb = tex2D(RT, iTexCoord).rgb;
	float sat = rgb.r + rgb.g + rgb.b;
	sat = sat / 3;
    return float4(sat,sat,sat,1.0);
}
