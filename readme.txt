Starting the game
*****************

1) Set correct debugger properties for the SpaceCrafts project

  - Open project properties of SpaceCrafts
    - Goto Configuration Properties > Debugging
      - Select "Debug" Configuration
      - Set working directory to: $(OutDir)
    - Select "Release" Configuration
      - Set working directory to: $(OutDir)

2) Start the game!


Key mapping
***********

Spacecraft control:

A,W,S,D  move
Space    fire

Camera control:

cursor   move free camera
tab      toogle follow player camera

Debug features:

1        toogle DebugDisplay
2        toogle display of Navigation Graph (DebugDisplay must be enabled)
3        toogle display log overlay

Game configuration
******************

Various parameter can be set in the media/game.cfg

ShowConfigDialog=[true|false]   skip Ogre configuration screen.
DragLook=[true|false]           only rotate free camera when mouse key is pressed.