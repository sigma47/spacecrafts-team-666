#ifndef __Path_h_
#define __Path_h_

// Simple class wrapping a path consisting of waypoints.
// - supportes normal and looped path
// - finds the closest point on the path
// - use a param to get any position on the path
//   param: 0 = startposition of the path
//   param: path length = endposition of the path
class Path
{
public:
	enum PathType
	{
		PATH_NORMAL,
		PATH_LOOP
	};

	Path();
	Path(const std::vector<Ogre::Vector3>& waypoints, PathType type);

	/// Findes the closest point of the given position on the graph.
	/// param lastParam: no points behind the lastParam are considered.
	/// returns the param to query the position of the path
	float getParam(const Ogre::Vector3& position, float lastParam = 0) const;

	/// Returns the position based on the param.
	Ogre::Vector3 getPosition(float param) const;

	/// Returns the total length of the path.
	float getTotalLength() const
	{
		return mTotalLength;
	}

	/// Returns true if the given param is at the end of the path
	/// Note: if the path is looped this will never return true
	bool isPathEnd(float param) const;

	/// Returns true if the path is empty or only has 1 waypoint.
	bool isEmpty() const { return mWaypoints.size() < 2; }

	void debugDraw() const;

private:
	float mapParam(float param) const;

	std::vector<Ogre::Vector3> mWaypoints;
	std::vector<float> mDistances;

	float mTotalLength;
	PathType mType;
};

#endif